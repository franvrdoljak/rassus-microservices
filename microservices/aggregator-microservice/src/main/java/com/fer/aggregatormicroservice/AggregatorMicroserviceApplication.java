package com.fer.aggregatormicroservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@SpringBootApplication
@RestController
@RefreshScope
public class AggregatorMicroserviceApplication implements CommandLineRunner {

	private final double KELVINS_IN_CELSIUS = 273.15;

	@Value("${temperature.unit: Default value}")
	private String temperatureUnit;

	@Value("${microservices.humidity: Default value}")
	private String humidityMicroserviceName;

	@Value("${microservices.temperature: Default value}")
	private String temperatureMicroserviceName;

	@Value("${microservices.aggregator: Default value}")
	private String aggregatorMicroserviceName;

	public static void main(String[] args) {
		SpringApplication.run(AggregatorMicroserviceApplication.class, args);
	}

	@Override
	public void run(String... args) {

		System.out.println(temperatureUnit);
		// TODO add logic for converting temperature values based on config unit

		System.out.println(humidityMicroserviceName);
		System.out.println(temperatureMicroserviceName);
		System.out.println(aggregatorMicroserviceName);
	}

	@GetMapping("/readings")
	public Reading getReadings(@RequestParam(required = false, name = "unit") TemperatureUnit wantedUnit){
		Reading fetchedReading = fetchReading();
		System.out.println(wantedUnit);
		if(wantedUnit == null ){
			return fetchedReading;
		}

		if(!temperatureUnit.equals(wantedUnit.toString())){
			if(wantedUnit.equals(TemperatureUnit.kelvin) && temperatureUnit.equals(TemperatureUnit.celsius.toString())){
				//pretvori iz celsius u kelvin
				fetchedReading.setTemperature(fetchedReading.getTemperature() + KELVINS_IN_CELSIUS);
			}

			if(wantedUnit.equals(TemperatureUnit.celsius) && temperatureUnit.equals(TemperatureUnit.kelvin.toString())){
				//pretvori iz celsius u kelvin
				fetchedReading.setTemperature(fetchedReading.getTemperature() - KELVINS_IN_CELSIUS);
			}
		}
		return fetchedReading;
	}

	private Reading fetchReading(){
		//dohvati urlove
		String temperatureMicroserviceURI = discoveryClient.getInstances(temperatureMicroserviceName).get(0).getUri().toString() + "/current-reading";
		String humidityMicroserviceURI = discoveryClient.getInstances(humidityMicroserviceName).get(0).getUri().toString() + "/current-reading";

		//dohvati temperaturu i vlaznost
		SensorReadingTemperature temperature = new RestTemplate().getForObject(temperatureMicroserviceURI, SensorReadingTemperature.class);
		SensorReadingHumidity humidity = new RestTemplate().getForObject(humidityMicroserviceURI, SensorReadingHumidity.class);

		return new Reading(temperature.getTemperature(), humidity.getHumidity());
	}

	@Autowired
	private DiscoveryClient discoveryClient;

	@RequestMapping("/service-instances/{applicationName}")
	public @ResponseBody
	List<ServiceInstance> serviceInstancesByApplicationName(
			@PathVariable String applicationName) {
		return this.discoveryClient.getInstances(applicationName);
	}

}
