package com.fer.aggregatormicroservice;

public enum TemperatureUnit {
    celsius, kelvin
}
