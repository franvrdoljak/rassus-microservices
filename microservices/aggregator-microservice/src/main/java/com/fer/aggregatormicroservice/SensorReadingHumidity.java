package com.fer.aggregatormicroservice;

public class SensorReadingHumidity {
    private Integer humidity;

    public SensorReadingHumidity() {
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "SensorReadingHumidity{" +
                "humidity='" + humidity + '\'' +
                '}';
    }
}
