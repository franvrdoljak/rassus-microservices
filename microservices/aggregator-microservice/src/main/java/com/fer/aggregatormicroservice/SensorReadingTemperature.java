package com.fer.aggregatormicroservice;

public class SensorReadingTemperature {
    private Double temperature;

    public SensorReadingTemperature() {
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "SensorReadingTemperature{" +
                "temperature=" + temperature +
                '}';
    }
}
