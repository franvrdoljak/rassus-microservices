package com.fer.temperaturemicroservice;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SensorReadingTemperature {
    @Id
    private Integer id;
    private Double temperature;

    public SensorReadingTemperature() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "SensorReadingTemperature{" +
                "id=" + id +
                ", temperature=" + temperature +
                '}';
    }
}
