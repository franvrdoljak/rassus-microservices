package com.fer.temperaturemicroservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalTime;
import java.util.Optional;

@RestController
public class SensorReadingTemperatureController {
    private SensorRepositoryTemperature repo;

    public SensorReadingTemperatureController(SensorRepositoryTemperature repo) {
        this.repo = repo;
    }

    @GetMapping("/current-reading")
    public Optional<SensorReadingTemperature> getCurrentReading() {
        LocalTime time = LocalTime.now();
        int id = time.getHour() * 4 + time.getMinute() / 15;
        return repo.findById(id);
    }
}
