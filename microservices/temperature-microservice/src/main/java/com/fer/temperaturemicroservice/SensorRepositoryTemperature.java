package com.fer.temperaturemicroservice;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SensorRepositoryTemperature extends JpaRepository<SensorReadingTemperature, Integer> {
}
