package com.fer.humiditymicroservice;

import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileReader;
import java.util.List;

@EnableDiscoveryClient
@SpringBootApplication
@RefreshScope
@RestController
public class HumidityMicroserviceApplication implements CommandLineRunner {

	@Autowired
	private SensorRepositoryHumidity repo;

	public static void main(String[] args) {
		SpringApplication.run(HumidityMicroserviceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {


		List<SensorReadingHumidity> readings = new CsvToBeanBuilder<SensorReadingHumidity>(new FileReader("/mjerenja.csv"))
				.withType(SensorReadingHumidity.class).build().parse();

		int i=0;
		for(SensorReadingHumidity reading: readings) {
			reading.setId(i++);
		}

		repo.saveAll(readings);

		readings.stream().forEach(System.out::println);
	}

	@RestController
	class ServiceInstanceRestController {

		@Autowired
		private DiscoveryClient discoveryClient;

		@RequestMapping("/service-instances/{applicationName}")
		public @ResponseBody List<ServiceInstance> serviceInstancesByApplicationName(
				@PathVariable String applicationName) {
			return this.discoveryClient.getInstances(applicationName);
		}
	}
}