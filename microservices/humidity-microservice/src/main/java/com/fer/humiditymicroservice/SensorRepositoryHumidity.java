package com.fer.humiditymicroservice;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SensorRepositoryHumidity extends JpaRepository<SensorReadingHumidity, Integer> {
}
