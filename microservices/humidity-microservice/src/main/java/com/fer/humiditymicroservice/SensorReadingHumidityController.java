package com.fer.humiditymicroservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalTime;
import java.util.Optional;

@RestController
public class SensorReadingHumidityController {
    private SensorRepositoryHumidity repo;

    public SensorReadingHumidityController(SensorRepositoryHumidity repo){
        this.repo = repo;
    }

    @GetMapping("/current-reading")
    public Optional<SensorReadingHumidity> getCurrentReading() {
        LocalTime time = LocalTime.now();
        int id = time.getHour() * 4 + time.getMinute() / 15;
        return repo.findById(id);
    }
}
