package com.fer.humiditymicroservice;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SensorReadingHumidity {
    @Id
    private Integer id;
    private Integer humidity;

    public SensorReadingHumidity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "SensorReadingHumidity{" +
                "id=" + id +
                ", humidity='" + humidity + '\'' +
                '}';
    }
}
